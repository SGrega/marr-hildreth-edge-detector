# Abstract #
The program written in MATLAB for detecting contours of human organs in the CT 
images from the CTMRI DB using the Marr-Hildreth edge detector. 



# More text in Slovene #

## Povzetek ##
Marr-Hildreth detektor robov na sliki deluje na podlagi Gaussovega glajenja in Laplacovim ostrenjem ter primernim mehanizmom za zaznavanje prehodov skozi nič. 

## Metode ##

Odkrivanje robov je sestavljeno iz treh osnovnih stopenj:

1. Glajenje slike za zmanjšanje šuma
2. Zaznavanje robnih točk (potencialne kandidate!)
3. Lokalizacija robov (izbira izmed kandidati, le tiste točke katere so resnično člani množice, ki sestavljajo rob)


Marr-Hildreth algoritem:

- Konvolucija LoG (Gaussov filter in nato Laplace) filtra z vhodno sliko.
- Odkrivanje *prehodov skozi nič* (ang "the zero crossings") za odkrivanje lokacij robov.
- (opcijsko) Povezovanje robov (ang. edge linking), namen katere je iz potencialnih točk robov narediti pravi rob in tako povezati sicer na videz razdrobljene točke na robu. 

glajenje (ang. blurring)= Gaussian filter
ostrenje, 2. odvod= Laplace

## Razno ##
Parametri sigma =1.399 in threshold so bili sprava izbrani naključno. Nato sem jih glede na odziv in teoretična priporočila (kot delež velikosti matrike) 
prilagajal za čim boljši rezultat. Dobri rezultati v tem primeru pomenijo čim boljšo odpravo šuma in čim natančnejši obris resničnih robov na sliki.

## Slike eksperimenta ##
![photo 1] (https://drive.google.com/uc?id=0B1yfSN5YNf0_LVFKdGpPUDl0dWM&export=download)


(The code will be published soon, I need a little time to clean up all internal things, meantime you can contact me and hopefully I will provide you the code)